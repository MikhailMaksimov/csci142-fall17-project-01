package elevator;

/**
 * Elevator class to simulate the operation of an elevator. Details
 * associated with opening and closing of the elevator doors, entry and
 * exit of people to and from the elevator, the number of people in, 
 * entering or leaving the elevator, and the timing of the movement of
 * the elevator are all "abstracted" out of the problem, encompassing 
 * all of these actions into a single "move()" operation.
 * 
 * @author Mikhail Maksimov
 */
public class Elevator implements ElevatorOperations
{
	public final static int NO_VALUE = -1; //used for un-set integers
	public final static boolean PRESSED = true; //used for in buttons
	
	private int myNumberOfFloors; //number of floors for the elevator
	private int myCurrentFloor; //current floor of the elevator
	private int myNextFloor; //next floor that the elevator should go to
	private int myDirection; //current direction of the elevator (updated every move())
	private int myOrderCounter; //number given to every input to order outside presses
	private int myOrderSequencer; //number used to check which outside button is pressed
	private int myPreviousUpFloor; //previous floor that up button was pressed on
	private int myPreviousDownFloor; //previous floor that down button was pressed on
	private int myPreviousInButton; //previous in button pressed
	
	private int[] myUpButtonRecorder; //up button array
	private int[] myDownButtonRecorder; //down button array
	private boolean[] myInButtonRecorder; //in button array
	
	
	/**
	 * Default constructor setting the number of floors for
	 * the elevator to five (5)
	 */
	public Elevator()
	{
		this(5);
	}


	/**
	 * Constructor setting the number of floors and setting up variables
	 * 
	 * @param numFloors total number of floors
	 */
	public Elevator(int numFloors)
	{
		myNumberOfFloors = numFloors;
		myCurrentFloor = 1;
		myOrderCounter = 0;
		myOrderSequencer = 1;
		myDirection = NOT_SET;
		myNextFloor = NO_VALUE;
		myPreviousUpFloor = NO_VALUE;
		myPreviousDownFloor = NO_VALUE;
		myPreviousInButton = NO_VALUE;
		
		myUpButtonRecorder = new int[myNumberOfFloors];	
		myDownButtonRecorder = new int[myNumberOfFloors];
		myInButtonRecorder = new boolean[myNumberOfFloors];

		for (int i = 0; i < numFloors; i++)
		{
			myUpButtonRecorder[i] = NO_VALUE;
			myDownButtonRecorder[i] = NO_VALUE;
		}
	}
	
	
	/**
	 * Method to simulating pushing the "up" button on the outside
	 * of an elevator on a given floor.
	 * 
	 * @param floor the floor that the button was pushed on
	 * @return true if the button could be pushed or false if it fails
	 */
	public boolean pushUp(int floor)
	{
		if(floor >= myNumberOfFloors || floor < 1)
		{
			return false;
		}
		if(floor == myPreviousUpFloor)
		{
			return false;
		}
		recordUp(floor);
		myPreviousUpFloor = floor; //for use next time person presses up
		return true;
	}
	
	
	/**
	 * Method for recording the UP inputs into an array and numbering their
	 * order with a counter
	 * 
	 * @param floor the floor that the up button was pressed on
	 */
	public void recordUp(int floor)
	{
		myOrderCounter++;	
		myUpButtonRecorder[floor-1] = myOrderCounter;
	}
	
	
	/**
	 * Method to simulating pushing the "down" button on the outside
	 * of an elevator on a given floor.
	 * 
	 * @param floor the floor that the button was pushed on
	 * @return true if the button could be pushed or false if it fails
	 */
	public boolean pushDown(int floor)
	{
		if(floor <= 1 || floor > myNumberOfFloors)
		{
			return false;
		}
		if (floor == myPreviousDownFloor)
		{
			return false;
		}
		recordDown(floor);
		myPreviousDownFloor = floor;
		return true;
	}
	
	
	/**
	 * Method for recording the DOWN inputs into an array and numbering their
	 * order with a counter
	 * 
	 * @param floor the floor that the down button was pressed on
	 */
	public void recordDown(int floor)
	{
		myOrderCounter++;
		myDownButtonRecorder[floor-1] = myOrderCounter; //[0,1,2,3,4]
	}
	
	
	/**
	 * Method to simulating pushing the floor button on the inside
	 * of an elevator.  Note that it doesn't matter the actual floor
	 * the elevator is on when this button is pushed.
	 * 
	 * @param floor the floor that the person wants to go to
	 * @return true if the button could be pushed or false if it fails
	 */
	public boolean pushIn(int floor)
	{
		if (floor > myNumberOfFloors || floor < 1)
		{
			return false;
		}
		if (floor == myCurrentFloor)
		{
			return false;
		}
		if (myDirection == UP && floor <= myCurrentFloor)
		{
			return false;
		}
		if (myDirection == DOWN && floor >= myCurrentFloor)
		{
			return false;
		}
		if (floor == myPreviousInButton)
		{
			return false;
		}
		recordIn(floor);
		myPreviousInButton = floor;
		return true;
	}
	
	
	/**
	 * Method for recording the button presses inside the elevator into
	 * an array. They are recorded as true denoted by PRESSED
	 * 
	 * @param floorPressed the number of the floor pressed inside the elevator
	 */
	public void recordIn(int floorPressed)
	{
		myInButtonRecorder[floorPressed-1] = PRESSED;
	}
	
	
	/**
	 * Move one step in the elevator process.
	 * 
	 * @return the floor the elevator is on after the step
	 */
	public int move()
	{
		if (myDirection == NOT_SET)
		{
			myNextFloor = findNotSet();
		}

		else if (myDirection == UP)
		{
			myNextFloor = findUp();
		}
		
		else if (myDirection == DOWN)
		{
			myNextFloor = findDown();
		}
		
		unsetPreviousFloors();	
		myCurrentFloor = myNextFloor;
		return myNextFloor;
	}
	
	
	/**
	 * Portion of the move() method for the direction NOT_SET that I made 
	 * into a separate method so that I can stop the FOR loop with "return"
	 * once it finds the next floor it should go to.
	 * 
	 * @return number of the floor that the elevator needs to go to next
	 */
	public int findNotSet()
	{
		for (int i = 1; i <= myNumberOfFloors; i++)
		{
			//what if up and down are pressed on same floor? Answer:
			if (myUpButtonRecorder[i-1] > 0 && myDownButtonRecorder[i-1] > 0) //up and down on same floor
			{
				if(myUpButtonRecorder[i-1] < myDownButtonRecorder[i-1])
				{
					if (myUpButtonRecorder[i-1] == myOrderSequencer)
					{
						myOrderSequencer++;
					}
					myDirection = UP;
					myUpButtonRecorder[i-1] = NO_VALUE;
					return i;
				}
				if(myUpButtonRecorder[i-1] > myDownButtonRecorder[i-1])
				{
					if (myDownButtonRecorder[i-1] == myOrderSequencer)
					{
						myOrderSequencer++;
					}		
					myDirection = DOWN;
					myDownButtonRecorder[i-1] = NO_VALUE;
					return i;
				}
			}
			if (myUpButtonRecorder[i-1] >= myOrderSequencer) //finds up button
			{
				if (myUpButtonRecorder[i-1] == myOrderSequencer)
				{
					myOrderSequencer++;
				}
				myDirection = UP;
				myUpButtonRecorder[i-1] = NO_VALUE;
				return i;
			}
			else if (myDownButtonRecorder[i-1] >= myOrderSequencer) //finds down button
			{
				if (myDownButtonRecorder[i-1] == myOrderSequencer)
				{
					myOrderSequencer++;
				}		
				myDirection = DOWN;
				myDownButtonRecorder[i-1] = NO_VALUE;
				return i;
			}
		}
		for (int j = myNumberOfFloors; j >= 1; j--) //finds in button
		{
			if (myInButtonRecorder[j-1] == PRESSED)
			{
				myInButtonRecorder[j-1] = false;
				myDirection = NOT_SET;
				myUpButtonRecorder[j-1] = NO_VALUE;
				myDownButtonRecorder[j-1] = NO_VALUE;
				return j;
			}
		}
		return myNextFloor;
	}
	
	
	/**
	 * Portion of the move() method for the direction UP that I made 
	 * into a separate method so that I can stop the FOR loop with "return"
	 * once it finds the next floor it should go to.
	 * 
	 * @return number of the floor that the elevator needs to go to next
	 */
	public int findUp()
	{
		for (int i = myCurrentFloor; i <= myNumberOfFloors; i++)
		{
			if (myInButtonRecorder[i-1] == PRESSED) // finds in button
			{
				myInButtonRecorder[i-1] = false;
				myDirection = NOT_SET;
				myUpButtonRecorder[i-1] = NO_VALUE;
				myDownButtonRecorder[i-1] = NO_VALUE;
				return i;
			}
			else if (myUpButtonRecorder[i-1] >= myOrderSequencer) //finds up button
			{
				if (myUpButtonRecorder[i-1] == myOrderSequencer)
				{
					myOrderSequencer++;
				}
				myUpButtonRecorder[i-1] = NO_VALUE;
				return i;
			}
		}
		for (int j = myNumberOfFloors; j > 1; j--)
		{
			if (myDownButtonRecorder[j-1] >= myOrderSequencer) //finds down button
			{
				myDirection = DOWN;
				myOrderSequencer++;
				myDownButtonRecorder[j-1] = NO_VALUE;
				return j;
			}
		}
		return myNextFloor;
	}
	
	
	/**
	 * Portion of the move() method for the direction DOWN that I made 
	 * into a separate method so that I can stop the FOR loop with "return"
	 * once it finds the next floor it should go to.
	 * 
	 * @return number of the floor that the elevator needs to go to next
	 */
	public int findDown()
	{
		for (int i = myCurrentFloor; i >= 1; i--)
		{
			if (myInButtonRecorder[i-1] == PRESSED) //finds in button
			{
				myInButtonRecorder[i-1] = false;
				myDirection = NOT_SET;
				myUpButtonRecorder[i-1] = NO_VALUE;
				myDownButtonRecorder[i-1] = NO_VALUE;
				return i;
			}
			else if (myDownButtonRecorder[i-1] >= myOrderSequencer) //finds down button
			{
				if (myDownButtonRecorder[i-1] == myOrderSequencer)
				{
					myOrderSequencer++;
				}
				myDownButtonRecorder[i-1] = NO_VALUE;
				return i;
			}
		}
		for (int j = 1; j <= myNumberOfFloors; j++)
		{
			if (myUpButtonRecorder[j-1] >= myOrderSequencer) //finds up button
			{
				myDirection = UP;
				myOrderSequencer++;
				myUpButtonRecorder[j-1] = NO_VALUE;
				return j;
			}
		}
		return myNextFloor;
	}
	
	
	/**
	 * Method for un-setting previous floor-remembering variables
	 * so that the corresponding floor can be pressed again because the 
	 * elevator has arrived at that floor. this corresponds to the rule
	 * that double-tapping a button will fail on the second tap
	 */
	public void unsetPreviousFloors()
	{
	    	
		if (myNextFloor == myPreviousUpFloor)
		{
			myPreviousUpFloor = NO_VALUE;
	    }
	    if (myNextFloor == myPreviousDownFloor)
	    {
			myPreviousDownFloor = NO_VALUE;
		}
	    if (myNextFloor == myPreviousInButton)
		{
			myPreviousInButton = NO_VALUE;
		}
	}

	
	/**
	 * Get the direction that is current set on the elevator.
	 * The direction does not update until the elevator moves
	 * 
	 * @return current direction of elevator
	 */
	public int getDirection()
	{
		return myDirection;
	}
	
	
	/**
	 * Get the floor that the elevator is on.  
	 * 
	 * @return the floor the elevator is on
	 */
	public int getFloor()
	{
		return myCurrentFloor;
	}
}
